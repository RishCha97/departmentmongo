import { Component } from '@angular/core';
import { NavController ,Platform, ToastController } from 'ionic-angular';

import { OrderService } from '../../providers/order-service';
import { OrderDetailPage } from '../order-detail/order-detail';

import { Toast } from 'ionic-native'

declare var window : any;

@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})
export class HomePage {
    data: any;
    orders: any = [];
    search = '';
    checkBox = [];
    orderString: string;
    responseString: boolean;
    constructor(public toastCtrl : ToastController,public platform : Platform ,public orderService: OrderService, public navCtrl: NavController) {
        this.getData();
    }

    /**
     * function to fetch data
     */
    getData() {
        console.log("-----------getData() function entered------------");
        this.orderService.getUnreleasedOrders().then((data) => {
            this.data = data;
            let temp = JSON.parse(this.data);
            console.log(temp);
            this.orders = [];
            for (let i in temp.orders) {
                this.orders.push(temp.orders[i])
            }
        })
        console.log("-----------getData() function exited------------");
    }

    /**
     * Release the selected Order
     */

    release() {
        console.log("-----------release() function entered------------");
        // this.orderService.releaseOrder(this.orderString).then((data) => {
        //     this.data = data;
        //     let temp = JSON.parse(this.data);
        //     console.log(temp);
        //     this.responseString = temp.responseStatus;
        // });
        this.platform.ready().then(() => {
            console.log("platform ready")
            Toast.show("success", 'short', 'top');  
        });
        let toast =this.toastCtrl.create({
            message: 'Success',
            duration: 3000
        });
        toast.present();
        

        console.log("-----------release() function exited------------");
    }

    /**
     *  Searchbar function
     */

    searchItems(ev: any) {

        let value = ev.target.value;

        if (value && value.trim() != this.search) {
            this.search = value;
            this.orders = this.orders.filter((item) => {
                return (item.orderNo.toLowerCase().indexOf(value.toLowerCase()) > -1);
            })
        } else {
            this.getData();
        }
    }

    /**
     * 
     */
    changePage(order){
        console.log("-----------release() function entered------------");

        this.navCtrl.push(OrderDetailPage , {OrderData : order});

        console.log("-----------release() function exited------------");
    }
}