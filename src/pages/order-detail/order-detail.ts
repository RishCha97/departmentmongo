import { Component } from '@angular/core';
import { NavController, NavParams, ToastController } from 'ionic-angular';

@Component({
  selector: 'page-order-detail',
  templateUrl: 'order-detail.html'
})
export class OrderDetailPage {

  orderData : any;

  constructor(public toastCtrl : ToastController,public navCtrl: NavController, public navParams: NavParams) {
    console.log("-----------constructor OrderDetailPage entered------------");
    this.orderData = this.navParams.get('OrderData');
    console.log(this.orderData);
    console.log("-----------constructor OrderDetailPage entered------------");
  }

  ionViewDidLoad() {
    
  }

    release() {
        console.log("-----------release() function entered------------");
        // this.orderService.releaseOrder(this.orderString).then((data) => {
        //     this.data = data;
        //     let temp = JSON.parse(this.data);
        //     console.log(temp);
        //     this.responseString = temp.responseStatus;
        // });
        let toast =this.toastCtrl.create({
            message: 'Success',
            duration: 3000
        });
        toast.present();
        

        console.log("-----------release() function exited------------");
    }

}
