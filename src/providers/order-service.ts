import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';

@Injectable()
export class OrderService {
    data: any;
    baseUrl: string;
    getUrl: string;
    postUrl: string;
    postString: string;
    constructor(public http: Http) {
        //this.baseUrl = 'order.json';
        this.baseUrl = 'http://172.29.220.209:9100/RestApp/resources/rest/';
    }

    /**
     * Get List of Unreleased Orders
     */
    getUnreleasedOrders() {
        console.log("----------getUnreleasedOrders() Function entered in OrderService-----------")
        // this.getUrl = this.baseUrl + 'orderByCmnyNStatus/1,3';
        // console.log('Url ::' + this.getUrl);
        // let headers = new Headers({ 'Content-Type': 'application/json' });
        // headers.append('Accept', 'application/json');
        // let options = new RequestOptions({ headers: headers });
        // return new Promise(resolve => {
        //     this.http.get(this.getUrl, options).subscribe(res => {
        //         this.data = res.json();
        //         console.log("data ==> " + this.data)
        //         console.log("----------getUnreleasedOrders() Function exited in OrderService-----------")
        //         resolve(JSON.stringify(this.data));
        //     });
        // });

        let headers = new Headers({ 'Content-Type': 'application/json' });
        headers.append('Accept', 'application/json');
        let options = new RequestOptions({ headers: headers });
        return new Promise((resolve)=>{

        this.http.get('order.json' , options).map(res => res.json()).subscribe((data)=>{
            this.data = data;
            resolve(JSON.stringify(this.data));
        })
        })


    }

    /**
     * Release Order
     */
    releaseOrder(orderString: string) {
        console.log("----------releaseOrder() Function entered in OrderService-----------");
        this.postUrl = this.baseUrl + 'releaseOrder/1,158436';
        console.log('postUrl = ' + this.postUrl);
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        let postParams = null;
        let body = JSON.stringify(postParams);
        console.log("----------releaseOrder() postParams-----------" + postParams);
        return new Promise(resolve => {
            this.http.post(this.postUrl, body, options)
                .map(res => res.json())
                .subscribe(data => {
                    resolve(data);
                });
        });

        // this.http.post(this.postUrl, null, options)
        //     .subscribe(data => {
        //         console.log(data['_pn_order_no']);
        //     }, error => {
        //         console.log(error);// Error getting the data
        //     });

    }

    /**
     * Form Post string for Order Release
     */
    getPostString() {
        console.log("----------getPostString() Function entered in OrderService-----------");
        this.postString = '{"_postupdateorder" : null}';
        console.log("----------getPostString() Function Exited in OrderService-----------");
        return this.postString;
    }

    /**
     * Generic method to extract response data
     */
    private extractData(res: Response) {
        console.log("----------extractData() Function entered in OrderService-----------");
        let body = res.json();
        console.log("----------extractData() body-----------" + body);
        console.log("----------extractData() Function exited in OrderService-----------");
        return body.data || {};
    }
    /**
     * Generic method to return Response error gracefully
     */

    private handleError(error: Response | any) {
        console.log("----------handleError() Function entered in OrderService-----------");
        // In a real world app, we might use a remote logging infrastructure
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error('errorMessage' + errMsg);
        console.log("----------handleError() Function exited in OrderService-----------");
        return Observable.throw(errMsg);
    }
}