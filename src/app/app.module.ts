import { NgModule, ErrorHandler } from '@angular/core';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { OrderDetailPage } from '../pages/order-detail/order-detail';

import { OrderService } from '../providers/order-service';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    OrderDetailPage
  ],
  imports: [
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    OrderDetailPage
  ],
  providers: [{provide: ErrorHandler, useClass: IonicErrorHandler}, OrderService]
})
export class AppModule {}
